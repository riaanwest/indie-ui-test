const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const server = browserSync.create();

sass.compiler = require('node-sass');

const paths = {
  html: {
    src: 'src/*.html',
    dest: 'dist'
  },
  scripts: {
    src: 'src/js/**/*.js',
    dest: 'dist/js'
  },
  styles: {
    src: 'src/scss/**/*.scss',
    dest: 'dist/css'
  },
  images: {
    src: 'src/images/*.*',
    dest: 'dist/images'
  }
};

function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.styles.dest));
}

function html() {
  return gulp.src(paths.html.src)
    .pipe(gulp.dest(paths.html.dest));
}

function images() {
  return gulp.src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
}

function scripts() {
  return gulp.src(paths.scripts.src)
    .pipe(gulp.dest(paths.scripts.dest));
}

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    server: {
      baseDir: './dist/'
    }
  });
  done();
}

const watch = () => {
  gulp.watch(paths.styles.src, gulp.series(styles, reload));
  gulp.watch(paths.images.src, gulp.series(images, reload));
  gulp.watch(paths.scripts.src, gulp.series(scripts, reload));
  gulp.watch(paths.html.src, gulp.series(html, reload));
}

const dev = gulp.series(styles, images, scripts, html, serve, watch);
exports.default = dev;
