(() => {
  const toggleBtn = document.querySelector('.nav-toggle');
  const nav = document.querySelector('.nav-items-container');

  toggleBtn.addEventListener('click', () => {
    nav.classList.toggle('is-showing');
  });
})()
